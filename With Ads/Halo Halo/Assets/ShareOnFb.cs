﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;

public class ShareOnFb : MonoBehaviour {
	

	void Awake()
		{
			if (!FB.IsInitialized)
			{
				FB.Init(InitCallback, OnHideUnity);
			}
			else
			{
	
				FB.ActivateApp();
	
			}
		}
 public void HELLO()
	{
		if (FB.IsInitialized) {
			FB.LogInWithReadPermissions (
				new List<string> (){ "public_profile", "email", "user_friends" },
				AuthCallback
			);
		}

	}
	private void InitCallback()
		{
			if (FB.IsInitialized)
			{
			FB.ActivateApp();

				if(FB.IsLoggedIn)
				{
				FB.ActivateApp();
				
				}	
			}
			else
			{
				
			}
			
		}
		
		private void OnHideUnity(bool isGameShown)
		{
			if (!isGameShown)
			{
				
				Time.timeScale = 0;
			}
			else
			{
				
				Time.timeScale = 1;
			}
		}

	private	void AuthCallback(IResult result)
		{
		if (FB.IsLoggedIn) {
			FB.LogInWithPublishPermissions (
				new List<string>(){"publish_actions"},
				CallBack
			);
		}
		}

	private  void CallBack(IResult result)
	{
		Debug.Log(result.RawResult + "Logged In");
	}



	public void FbShare()
	{
		StartCoroutine (TakeScreenshot ());
	}
	private IEnumerator TakeScreenshot()
	{
		yield return new WaitForEndOfFrame();

		var width = Screen.width;
		var height = Screen.height;
		var tex = new Texture2D(width, height, TextureFormat.RGB24, false);
		// Read screen contents into the texture
		tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
		tex.Apply();
		byte[] screenshot = tex.EncodeToPNG();

		var wwwForm = new WWWForm();
		wwwForm.AddBinaryData("image", screenshot, "Screenshot.png");
		//wwwForm.AddField ("name", caption);
		FB.API("me/photos", HttpMethod.POST, APICallback, wwwForm);
	}

	void APICallback(IGraphResult result){
		
		Debug.Log (result.ToString ());
	}
}
