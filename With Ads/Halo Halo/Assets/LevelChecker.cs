﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelChecker : MonoBehaviour {
	public Animator[] LevelAnimation1;

	// Use this for initialization
	void Start () {
		if (PlayerPrefs.HasKey ("LEV") == false) {
			PlayerPrefs.SetInt ("LEV", 1);
		} else {
			PlayerPrefs.GetInt ("LEV");
		}
	}
	
	// Update is called once per frame
	void Update () {
		CheckYourLevel ();
	}


	public void CheckYourLevel()
	{
		if (PlayerPrefs.GetInt ("LEV") <= 5) 
		{
			LevelAnimation1 [0].SetBool ("PlayingBig", true);
		}

		if (PlayerPrefs.GetInt ("LEV") >= 6 && PlayerPrefs.GetInt ("LEV") <= 10) {
			LevelAnimation1 [1].SetBool ("PlayingBig", true);
		}
		if (PlayerPrefs.GetInt ("LEV") >= 11 && PlayerPrefs.GetInt ("LEV") <= 15) {

			LevelAnimation1 [2].SetBool ("PlayingBig", true);

		}
		if (PlayerPrefs.GetInt ("LEV") >= 16 && PlayerPrefs.GetInt ("LEV") <= 20) {

			LevelAnimation1 [3].SetBool ("PlayingBig", true);

		}
		if (PlayerPrefs.GetInt ("LEV") >= 21 && PlayerPrefs.GetInt ("LEV") <= 25) {

			LevelAnimation1 [4].SetBool ("PlayingBig", true);

		}
		if (PlayerPrefs.GetInt ("LEV") >= 26 && PlayerPrefs.GetInt ("LEV") <= 30) {

			LevelAnimation1 [5].SetBool ("PlayingBig", true);

		
		}
		if (PlayerPrefs.GetInt ("LEV") >= 31 && PlayerPrefs.GetInt ("LEV") <= 35) {

			LevelAnimation1 [6].SetBool ("PlayingBig", true);

		}


	}
}
