using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.ComponentModel;


public class AnimationAssistant : MonoBehaviour {
	// This script is responsible for procedural animations in the game. Such as change of place 2 chips and the effect of the explosion.
	public GameObject ChumchumIdle;
	public GameObject[] Music;
	public GameObject[] Dance;
	public GameObject[] Art;
	public GameObject[] Food;
	public GameObject[] Beauty;
	public GameObject[] Travel;
	public GameObject[] Sports;

	Animator anim;
	public static AnimationAssistant main; // Main instance. Need for quick access to functions.
	void  Awake (){
		main = this;
	}

	float swapDuration = 0.2f;

	// Temporary Variables
	bool swaping = false; // ИTRUE when the animation plays swapping 2 chips
	[HideInInspector]
	public bool forceSwap = false; // Forced change of place of chips, ignoring matching.


	void Start() {
		BombMixEffect.Initialize (); // Initialization recipes mixing bombs
	}

	// Function immediate swapping 2 chips
	public void SwapTwoItemNow (Chip a, Chip b) {
		if (!a || !b) return;
		if (a == b) return;
		if (a.parentSlot.slot.GetBlock() || b.parentSlot.slot.GetBlock()) return;

		Vector3 posA = a.parentSlot.transform.position;
		Vector3 posB = b.parentSlot.transform.position;
		
		a.transform.position = posB;
		b.transform.position = posA;
		
		a.movementID = SessionAssistant.main.GetMovementID();
		b.movementID = SessionAssistant.main.GetMovementID();
		
		SlotForChip slotA = a.parentSlot;
		SlotForChip slotB = b.parentSlot;
		
		slotB.SetChip(a);
		slotA.SetChip(b);
	}

	// The function of swapping 2 chips
	public void SwapTwoItem (Chip a, Chip b) {
		if (!SessionAssistant.main.isPlaying) return;
		StartCoroutine (SwapTwoItemRoutine(a, b)); // Starting corresponding coroutine
	}


	// Coroutine swapping 2 chips
	IEnumerator SwapTwoItemRoutine (Chip a, Chip b){
		// cancellation terms
		if (swaping) yield break; // If the process is already running
		if (!a || !b) yield break; // If one of the chips is missing
		if (a.parentSlot.slot.GetBlock() || b.parentSlot.slot.GetBlock()) yield break; // If one of the chips is blocked

		if (!SessionAssistant.main.CanIAnimate()) yield break; // If the core prohibits animation
		switch (LevelProfile.main.limitation) {
			case Limitation.Moves:
				if (SessionAssistant.main.movesCount <= 0) yield break; break; // If not enough moves
			case Limitation.Time:
				if (SessionAssistant.main.timeLeft <= 0) yield break; break; // If not enough time
		}

		bool mix = false; // The effect of mixing or not
		if (BombMixEffect.ContainsPair (a.chipType, b.chipType)) // Checking the possibility of mixing
						mix = true;

		int move = 0; // Number of points movement which will be expend
		
		SessionAssistant.main.animate++;
		swaping = true;
		
		Vector3 posA = a.parentSlot.transform.position;
		Vector3 posB = b.parentSlot.transform.position;
		
		float progress = 0;

        Vector3 normal = a.parentSlot.slot.x == b.parentSlot.slot.x ? Vector3.right : Vector3.up;

		// Animation swapping 2 chips
		while (progress < swapDuration) {
            a.transform.position = Vector3.Lerp(posA, posB, progress / swapDuration) + normal * Mathf.Sin(3.14f * progress / swapDuration) * 0.2f;
			if (!mix) b.transform.position = Vector3.Lerp(posB, posA, progress/swapDuration) - normal * Mathf.Sin(3.14f * progress / swapDuration) * 0.2f;
			
			progress += Time.deltaTime;
			
			yield return 0;
		}
		
		a.transform.position = posB;
		if (!mix) b.transform.position = posA;
		
		a.movementID = SessionAssistant.main.GetMovementID();
		b.movementID = SessionAssistant.main.GetMovementID();
		
		if (mix) { // Scenario mix effect
			swaping = false;
			BombPair pair = new BombPair(a.chipType, b.chipType);
			SlotForChip slot = b.parentSlot;
			a.HideChip();
			b.HideChip();
			BombMixEffect.Mix(pair, slot);
			SessionAssistant.main.movesCount--;
			SessionAssistant.main.animate--;
			yield break;
		}

		// Scenario the effect of swapping two chips
		SlotForChip slotA = a.parentSlot;
		SlotForChip slotB = b.parentSlot;
		
		slotB.SetChip(a);
		slotA.SetChip(b);
		
		
		move++;

		// searching for solutions of matching
		int count = 0; 
		SessionAssistant.Solution solution;
		
		solution = slotA.MatchAnaliz();
		if (solution != null) count += solution.count;
		
		solution = slotB.MatchAnaliz();
		if (solution != null) count += solution.count;
		
		// Scenario canceling of changing places of chips
		if (count == 0 && !forceSwap) {
			AudioAssistant.Shot("SwapFailed");
			while (progress > 0) {
                a.transform.position = Vector3.Lerp(posA, posB, progress / swapDuration) - normal * Mathf.Sin(3.14f * progress / swapDuration) * 0.2f;
                b.transform.position = Vector3.Lerp(posB, posA, progress / swapDuration) + normal * Mathf.Sin(3.14f * progress / swapDuration) * 0.2f;
				
				progress -= Time.deltaTime;
				
				yield return 0;
			}
			
			a.transform.position = posA;
			b.transform.position = posB;
			
			a.movementID = SessionAssistant.main.GetMovementID();
			b.movementID = SessionAssistant.main.GetMovementID();
			
			slotB.SetChip(b);
			slotA.SetChip(a);
			
			move--;
		} else {
			AudioAssistant.Shot("SwapSuccess");
			SessionAssistant.main.swapEvent ++;

			if (LevelProfile.main.level <= 5) // Detecting Level 1 - 5 
			{

				int MusicOut = Random.Range (0, Music.Length);

				if (MusicOut ==1) {
					Music [0].SetActive (false);
					Music [1].SetActive (true);
					Music [2].SetActive (false);
					Music [3].SetActive (false);
					Music [4].SetActive (false);

				}
				if (MusicOut ==2) {
					Music [0].SetActive (false);
					Music [1].SetActive (false);
					Music [2].SetActive (true);
					Music [3].SetActive (false);
					Music [4].SetActive (false);

				}
				if (MusicOut ==3) {
					Music [0].SetActive (false);
					Music [1].SetActive (false);
					Music [2].SetActive (false);
					Music [3].SetActive (true);
					Music [4].SetActive (false);

				}
				if (MusicOut ==4) {
					Music [0].SetActive (false);
					Music [1].SetActive (false);
					Music [2].SetActive (false);
					Music [3].SetActive (false);
					Music [4].SetActive (true);

				}
				if (MusicOut ==0) {
					Music [0].SetActive (true);
					Music [1].SetActive (false);
					Music [2].SetActive (false);
					Music [3].SetActive (false);
					Music [4].SetActive (false);

				}

				ChumchumIdle.SetActive (false);
				//				Listofimage [0].SetActive (true);
				//				Listofimage [7].SetActive (false);
				StartCoroutine (Removers());
				//	anim.SetBool ("music", true);
				Debug.Log ("Tama na");
			}

			if (LevelProfile.main.level >= 6 && LevelProfile.main.level <= 10) // Detecting Level 6-10
			{

				int DanceOut = Random.Range (0, Dance.Length);

				if (DanceOut ==1) {
					Dance [0].SetActive (false);
					Dance [1].SetActive (true);
					Dance [2].SetActive (false);
					Dance [3].SetActive (false);
					Dance [4].SetActive (false);

				}
				if (DanceOut ==2) {
					Dance [0].SetActive (false);
					Dance [1].SetActive (false);
					Dance [2].SetActive (true);
					Dance [3].SetActive (false);
					Dance [4].SetActive (false);

				}
				if (DanceOut ==3) {
					Dance [0].SetActive (false);
					Dance [1].SetActive (false);
					Dance [2].SetActive (false);
					Dance [3].SetActive (true);
					Dance [4].SetActive (false);

				}
				if (DanceOut ==4) {
					Dance [0].SetActive (false);
					Dance [1].SetActive (false);
					Dance [2].SetActive (false);
					Dance [3].SetActive (false);
					Dance [4].SetActive (true);

				}
				if (DanceOut ==0) {
					Dance [0].SetActive (true);
					Dance [1].SetActive (false);
					Dance [2].SetActive (false);
					Dance [3].SetActive (false);
					Dance [4].SetActive (false);

				}
				ChumchumIdle.SetActive (false);
				//				Listofimage [1].SetActive (true);
				//				Listofimage [7].SetActive (false);
				//	anim.SetBool ("dance", true);
				StartCoroutine (Removers());
			}

			if (LevelProfile.main.level >= 11 && LevelProfile.main.level <= 15) // Detecting Level 11 - 15 
			{

				int Artout = Random.Range (0, Art.Length);
				if (Artout ==1) {
					Art [0].SetActive (false);
					Art [1].SetActive (true);
					Art [2].SetActive (false);
					Art [3].SetActive (false);
					Art [4].SetActive (false);

				}
				if (Artout ==2) {
					Art [0].SetActive (false);
					Art [1].SetActive (false);
					Art [2].SetActive (true);
					Art [3].SetActive (false);
					Art [4].SetActive (false);

				}
				if (Artout ==3) {
					Art [0].SetActive (false);
					Art [1].SetActive (false);
					Art [2].SetActive (false);
					Art [3].SetActive (true);
					Art [4].SetActive (false);

				}
				if (Artout ==4) {
					Art [0].SetActive (false);
					Art [1].SetActive (false);
					Art [2].SetActive (false);
					Art [3].SetActive (false);
					Art [4].SetActive (true);

				}
				if (Artout ==0) {
					Art [0].SetActive (true);
					Art [1].SetActive (false);
					Art [2].SetActive (false);
					Art [3].SetActive (false);
					Art [4].SetActive (false);

				}
				ChumchumIdle.SetActive (false);
				//				Listofimage [2].SetActive (true);
				//				Listofimage [7].SetActive (false);
				//	anim.SetBool ("art", true);
				StartCoroutine (Removers());
			}

			if (LevelProfile.main.level >= 21 && LevelProfile.main.level <= 25) // Detecting Level 16 - 20 
			{

				int FoodOut = Random.Range (0, Food.Length);
				if (FoodOut ==1) {
					Food [0].SetActive (false);
					Food [1].SetActive (true);
					Food [2].SetActive (false);
				

				}
				if (FoodOut ==2) {
					Food [0].SetActive (false);
					Food [1].SetActive (false);
					Food [2].SetActive (true);


				}
				if (FoodOut ==0) {
					Food [0].SetActive (true);
					Food [1].SetActive (false);
					Food [2].SetActive (false);


				}

				ChumchumIdle.SetActive (false);
				StartCoroutine (Removers());
			}
			if (LevelProfile.main.level >= 16 && LevelProfile.main.level <= 20) 
			{
				int BeautyOut = Random.Range (0, Beauty.Length);
				if (BeautyOut == 1) {
					Beauty [0].SetActive (false);
					Beauty [1].SetActive (true);
					Beauty [2].SetActive (false);
					Beauty [3].SetActive (false);
				

				}
				if (BeautyOut ==2) {
					Beauty [0].SetActive (false);
					Beauty [1].SetActive (false);
					Beauty [2].SetActive (true);
					Beauty [3].SetActive (false);
				

				}
				if (BeautyOut ==3) {
					Beauty [0].SetActive (false);
					Beauty [1].SetActive (false);
					Beauty [2].SetActive (false);
					Beauty [3].SetActive (true);


				}
				if (BeautyOut ==0) {
					Beauty [0].SetActive (true);
					Beauty [1].SetActive (false);
					Beauty [2].SetActive (false);
					Beauty [3].SetActive (false);


				}
				if (BeautyOut ==0) {
					Beauty [0].SetActive (true);
					Beauty [1].SetActive (false);
					Beauty [2].SetActive (false);
					Beauty [3].SetActive (false);


				}
				ChumchumIdle.SetActive (false);
				//anim.SetBool ("food", true);
				StartCoroutine (Removers());
			}

			if (LevelProfile.main.level >= 26 && LevelProfile.main.level <= 30) 
			{
				int TravelOut = Random.Range (0, Travel.Length);
				if (TravelOut ==1) {
					Travel [0].SetActive (false);
					Travel [1].SetActive (true);
					Travel [2].SetActive (false);
					Travel [3].SetActive (false);
					Travel [4].SetActive (false);

				}
				if (TravelOut ==2) {
					Travel [0].SetActive (false);
					Travel [1].SetActive (false);
					Travel [2].SetActive (true);
					Travel [3].SetActive (false);
					Travel [4].SetActive (false);

				}
				if (TravelOut ==3) {
					Travel [0].SetActive (false);
					Travel [1].SetActive (false);
					Travel [2].SetActive (false);
					Travel [3].SetActive (true);
					Travel [4].SetActive (false);

				}
				if (TravelOut ==4) {
					Travel [0].SetActive (false);
					Travel [1].SetActive (false);
					Travel [2].SetActive (false);
					Travel [3].SetActive (false);
					Travel [4].SetActive (true);

				}
				if (TravelOut ==0) {
					Travel [0].SetActive (true);
					Travel [1].SetActive (false);
					Travel [2].SetActive (false);
					Travel [3].SetActive (false);
					Travel [4].SetActive (false);

				}
				ChumchumIdle.SetActive (false);
				StartCoroutine (Removers());
			}

			if (LevelProfile.main.level >= 31 && LevelProfile.main.level <= 35) 
			{
				int SportOut = Random.Range (0, Sports.Length);
				if (SportOut ==1) {
					Sports [0].SetActive (false);
					Sports [1].SetActive (true);
					Sports [2].SetActive (false);
					Sports [3].SetActive (false);
					Sports [4].SetActive (false);

				}
				if (SportOut ==2) {
					Sports [0].SetActive (false);
					Sports [1].SetActive (false);
					Sports [2].SetActive (true);
					Sports [3].SetActive (false);
					Sports [4].SetActive (false);

				}
				if (SportOut ==3) {
					Sports [0].SetActive (false);
					Sports [1].SetActive (false);
					Sports [2].SetActive (false);
					Sports [3].SetActive (true);
					Sports [4].SetActive (false);

				}
				if (SportOut ==4) {
					Sports [0].SetActive (false);
					Sports [1].SetActive (false);
					Sports [2].SetActive (false);
					Sports [3].SetActive (false);
					Sports [4].SetActive (true);

				}
				if (SportOut ==0) {
					Sports [0].SetActive (true);
					Sports [1].SetActive (false);
					Sports [2].SetActive (false);
					Sports [3].SetActive (false);
					Sports [4].SetActive (false);

				}
				ChumchumIdle.SetActive (false);
				StartCoroutine (Removers());
			}

		}

        SessionAssistant.main.firstChipGeneration = false;

		SessionAssistant.main.movesCount -= move;
		SessionAssistant.main.EventCounter ();
		
		SessionAssistant.main.animate--;
		swaping = false;
	}

	IEnumerator Removers()
	{
		float sec = 3f;
		yield return new WaitForSeconds(sec);

		if (LevelProfile.main.level <= 5) 
		{

			Music [0].SetActive (false);
			Music [1].SetActive (false);
			Music [2].SetActive (false);
			Music [3].SetActive (false);
			Music [4].SetActive (false);
			ChumchumIdle.SetActive (true);
		}

		if (LevelProfile.main.level >= 6 && LevelProfile.main.level <= 10) 
		{

			Dance [0].SetActive (false);
			Dance [1].SetActive (false);
			Dance [2].SetActive (false);
			Dance [3].SetActive (false);
			Dance [4].SetActive (false);
			ChumchumIdle.SetActive (true);
		}

		if (LevelProfile.main.level >= 11 && LevelProfile.main.level <= 15) 
		{
			Art [0].SetActive (false);
			Art [1].SetActive (false);
			Art [2].SetActive (false);
			Art [3].SetActive (false);
			Art [4].SetActive (false);
			ChumchumIdle.SetActive (true);
		}

		if (LevelProfile.main.level >= 21 && LevelProfile.main.level <= 25) 
		{
			Food [0].SetActive (false);
			Food [1].SetActive (false);
			Food [2].SetActive (false);
			Food [3].SetActive (false);
			Food [4].SetActive (false);
			ChumchumIdle.SetActive (true);
		}
		if (LevelProfile.main.level >= 16 && LevelProfile.main.level <= 20) 
		{
			Beauty [0].SetActive (false);
			Beauty [1].SetActive (false);
			Beauty [2].SetActive (false);
			Beauty [3].SetActive (false);
			Beauty [4].SetActive (false);
			ChumchumIdle.SetActive (true);
		}

		if (LevelProfile.main.level >= 26 && LevelProfile.main.level <= 30) 
		{
			Travel [0].SetActive (false);
			Travel [1].SetActive (false);
			Travel [2].SetActive (false);
			Travel [3].SetActive (false);
			Travel [4].SetActive (false);
			ChumchumIdle.SetActive (true);
		}

		if (LevelProfile.main.level >= 31 && LevelProfile.main.level <= 35) 
		{
			Sports [0].SetActive (false);
			Sports [1].SetActive (false);
			Sports [2].SetActive (false);
			Sports [3].SetActive (false);
			Sports [4].SetActive (false);
			ChumchumIdle.SetActive (true);
		}


	}


	// Function of creating of explosion effect
	public void  Explode (Vector3 center, float radius, float force){
		Chip[] chips = GameObject.FindObjectsOfType<Chip>();
		Vector3 impuls;
		foreach(Chip chip in chips) {
			if ((chip.transform.position - center).magnitude > radius) continue;
			impuls = (chip.transform.position - center) * force;
			impuls *= Mathf.Pow((radius - (chip.transform.position - center).magnitude) / radius, 2);
			chip.impulse += impuls;
		}
	}

	public void TeleportChip(Chip chip, Slot target) {
		StartCoroutine (TeleportChipRoutine (chip, target));
	}

	IEnumerator TeleportChipRoutine (Chip chip, Slot target) {
		if (!chip.parentSlot) yield break;

		TrailRenderer trail = chip.gameObject.GetComponentInChildren<TrailRenderer> ();
		float trailTime = 0;
		
		if (trail) {
			trailTime = trail.time;
			trail.time = 0;
		}

		float defScale = chip.transform.localScale.x;
		float scale = defScale;

//		chip.animation.Play("Minimizing");
//
//		while (chip.animation.isPlaying) {
//			yield return 0;
//		}

		while (scale > 0f) {
			scale -= Time.deltaTime * 10f;
			chip.transform.localScale = Vector3.one * scale;
			yield return 0;
		}


		if (!target.GetChip () && chip && chip.parentSlot) {
			Transform a = chip.parentSlot.transform;
			Transform b = target.transform;

            Color color;
            if (chip.id == Mathf.Clamp(chip.id, 0, 5))
                color = Chip.colors[chip.id];
            else
                color = Color.white;

            Lightning l = Lightning.CreateLightning(5, a, b, color);

			target.SetChip(chip);

			chip.transform.position = chip.parentSlot.transform.position;
			yield return 0;
			l.end = null;
		}

		yield return 0;


		if (trail) {
			trail.time = trailTime;
		}

		scale = 0.2f;
		while (scale < defScale) {
			scale += Time.deltaTime * 10f;
			chip.transform.localScale = Vector3.one * scale;
			yield return 0;
		}
		
		chip.transform.localScale = Vector3.one * defScale;
	}
}