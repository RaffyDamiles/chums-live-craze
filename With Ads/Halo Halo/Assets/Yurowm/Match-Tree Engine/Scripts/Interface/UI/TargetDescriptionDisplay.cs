﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent (typeof (Text))]
public class TargetDescriptionDisplay : MonoBehaviour {
	
	Text text;
	
	void Awake () {
		text = GetComponent<Text> ();	
	}
	
	void OnEnable () {
		if (LevelProfile.main == null) {
			text.text = "";
			return;
		}
		string descrition = "";

		switch (LevelProfile.main.target) {
			case FieldTarget.Score: descrition += "You should get " + LevelProfile.main.firstStarScore.ToString() + " score points And "; break;
			case FieldTarget.Jelly: descrition += "You should  destroy all jellies in this level."; break;
			case FieldTarget.Block: descrition += "You should  destroy all blocks in this level."; break;
			case FieldTarget.Color: descrition += "You should  destroy the certain count of chips of certain color"; break;
            case FieldTarget.SugarDrop: descrition += "You should  drop all sugar chips"; break;
        }

		descrition += " ";

		switch (LevelProfile.main.limitation) {
		case Limitation.Moves: descrition += "You only have " + LevelProfile.main.moveCount.ToString() + " moves to Pass this Level."; break;
		case Limitation.Time: descrition += "You only have " + Timer.ToTimerFormat(LevelProfile.main.duraction) + " of time to do this."; break;
		}

		text.text = descrition;
	}
}
