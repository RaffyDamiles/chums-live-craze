﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Purchasing;

public class IAPManager : MonoBehaviour ,IStoreListener
{

	public static IAPManager Instance; 


	static IStoreController m_storeController;
	static IExtensionProvider m_storeExtensionProvider;

	int m_Coins = 0;
	public int Coins
	{

		get
		{
			return  m_Coins;
		}
	}

	public static string productId40Coins = "com.cdu.chumslivecraze.40_coins";
	public static string productId60Coins = "com.cdu.chumslivecraze.60_coins";
	public static string productId100Coins = "com.cdu.chumslivecraze.100_coins";
	public static string productId200Coins = "com.cdu.chumslivecraze.200_coins";

	public Text CoinText;
	public Text MessageText;

	void Awake()
	{
		if (Instance == null) 
		{
			Instance = this;
			DontDestroyOnLoad (gameObject);
		} else 
		{
			Destroy (gameObject);
		}
	}

	void Start()
	{
		if (!IsInitialized ()) {

			initializedPurchasing ();
		}

	}
	//----------------------------------
	public void initializedPurchasing()
	{
		if (IsInitialized ()) {
			return;
		}
		StandardPurchasingModule module = StandardPurchasingModule.Instance ();

		ConfigurationBuilder builder = ConfigurationBuilder.Instance (module);
		builder.AddProduct (productId40Coins, ProductType.Consumable);
		builder.AddProduct (productId60Coins, ProductType.Consumable);
		builder.AddProduct (productId100Coins, ProductType.Consumable);
		builder.AddProduct (productId200Coins, ProductType.Consumable);
		UnityPurchasing.Initialize (this, builder);
	}
	//----------------------------------
	public void OnInitialized(IStoreController controller,IExtensionProvider extensions)
	{
		m_storeController = controller;
		m_storeExtensionProvider = extensions;
	}
	//----------------------------------
	public void OnInitializeFailed(InitializationFailureReason error)
	{

		Debug.Log ("IAPMANAGER OnInitializedFailed Error : " + error);
	}
	//----------------------------------
	bool IsInitialized ()
	{
		return m_storeController != null && m_storeExtensionProvider != null;
	}

	public void OnPurchaseFailed(Product i, PurchaseFailureReason p)
	{
		Debug.Log ("IAPMANAGER OnPurchaseFailed for Product : " + i.definition.storeSpecificId + "PurchaseFailureReason : "+ p);
	}

	void EnableProcduct (string productId)
	{
	if (productId == productId40Coins) {
		AddCoins (40);
	}
	else if (productId == productId60Coins) {
		AddCoins (60);
	}
	else if (productId == productId100Coins) {
			AddCoins (100);
	}
	else if (productId == productId200Coins) {
			AddCoins (200);
	}
	}

	//----------------------------------
	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
	{
		string productId = e.purchasedProduct.definition.id;

	
		return PurchaseProcessingResult.Complete;
	}

	//----------------------------------
	public void BuyProductID(string productId)
	{
		if (IsInitialized ()) {
			Product product = m_storeController.products.WithID (productId);
			if (product != null) {
				if (product.hasReceipt) {
					EnableProcduct (productId);
					Debug.Log ("IAPMANAGER Purchasing product : " + product.definition.id);
				}

			} else if (product.availableToPurchase) {
				m_storeController.InitiatePurchase(product);
				Debug.Log ("IAPMANAGER Purchasing product : " + product.definition.id);

			}
			else
			{
				Debug.Log ("IAPMANAGER BuyProductID  Error : Product " + productId +"not Found");
			}

		} 
		else 
		{
			Debug.Log ("IAPMANAGER BuyProductID  Error : " + productId +". Store not initialized");
		}

	}
	//----------------------------------
	public void AddCoins(int CoinsToAdd)
	{
		m_Coins += CoinsToAdd;
	}
	//----------------------------------
	public void Buy40Coins()
	{
		BuyProductID (productId40Coins);
	}

	public void Buy60Coins()
	{
		BuyProductID (productId60Coins);
	}
	public void Buy100Coins()
	{
		BuyProductID (productId100Coins);
	}
	public void Buy200Coins()
	{
		BuyProductID (productId200Coins);
	}

	//----------------------------------
	void UpdateCoinText()
	{
		if (CoinText != null) {
			CoinText.text = "Coins :" + m_Coins.ToString ();
		}
	}


	void ShowMessage(string message, float delay =2f)
	{
		StartCoroutine(ShowMessageRoutine(message,delay));

	}

	IEnumerator ShowMessageRoutine(string message, float delay)
	{

		if (MessageText != null) {
			MessageText.text = message;
	
			yield return new WaitForSecondsRealtime (delay);
			MessageText.text = "";
		}
		
	}
}
