﻿using UnityEngine;
using System.Collections;

public class NextLevel : MonoBehaviour {
	public GameObject lastLevel;
	public GameObject region1;
	public bool goOnce;
	// Update is called once per frame

	void Start(){

		goOnce = false;

	}
	void Update () {
	
		if(lastLevel.activeSelf && goOnce == false){

			region1.SetActive(true);
			goOnce = true;
	   }
}

}
