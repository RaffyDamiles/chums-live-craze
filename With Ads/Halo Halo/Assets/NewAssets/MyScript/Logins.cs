﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using UnityEngine.UI;
using System.Linq;


public class Logins : MonoBehaviour {

	
	public GameObject dialogLoggedIn;
	public GameObject dialogLoggedOut;
	public GameObject DialogUsername;
	public GameObject DialogProfilePic;
	
	void Awake()
	{
		if (!FB.IsInitialized)
		{
			FB.Init(InitCallback, OnHideUnity);
		}
		else
		{

			FB.ActivateApp();

		}
	}
	
	private void InitCallback()
	{
		if (FB.IsInitialized)
		{
			if(FB.IsLoggedIn)
			{
			FB.ActivateApp();
			
			}	
		}
		else
		{
			
		}
		DealWithFBMenus(FB.IsLoggedIn);
	}
	
	private void OnHideUnity(bool isGameShown)
	{
		if (!isGameShown)
		{
			
			Time.timeScale = 0;
		}
		else
		{
			
			Time.timeScale = 1;
		}
	}
	
	
	
	
	
	public void LogIn()
	{
		List<string> permissions =new List<string>();
		permissions.Add("public_profile");
		FB.LogInWithReadPermissions(permissions,AuthCallBack);
		if (AccessToken.CurrentAccessToken.Permissions.Contains("publish_actions")) {
			Debug.Log("have publish actions");

		} else {
			Debug.Log("no publish actions");
		}
	}
	
	void AuthCallBack(IResult result)
	{
		
		
		if(result.Error != null)
		{
			Debug.Log (result.Error);
			
		}
		
		else
		{
			if (FB.IsLoggedIn)
			{
				
				//	FB.ActivateApp();
				Debug.Log ("IS Logged In");		
			}
			else
			{
				Debug.Log ("IS not Logged In");		
			}
			DealWithFBMenus(FB.IsLoggedIn);
		}
	}
	
	void DealWithFBMenus(bool IsLoggedIn)
	{
		if(IsLoggedIn)
		{
			
			dialogLoggedIn.SetActive(true);
			dialogLoggedOut.SetActive(false);
			FB.API ("/me?fields=first_name",HttpMethod.GET,DisplayUserName);
			FB.API ("/me/picture?type=square&height=128&width=128",HttpMethod.GET,DisplayProfilePic);
			
		}
		else
		{

			dialogLoggedIn.SetActive(false);
			dialogLoggedOut.SetActive(true);
		}
	}
	
	
	void DisplayUserName(IResult result)
	{
		Text UserName = DialogUsername.GetComponent<Text>();
		
		
		if(result.Error ==null)
		{
			UserName.text = "Hi There, " + result.ResultDictionary["first_name"];
		}
		else
		{
			Debug.Log(result.Error);
			
		}
	}
	
	void DisplayProfilePic(IGraphResult result)
	{
		if(result.Texture !=null)
		{
			Image ProfilePic = DialogProfilePic.GetComponent<Image>();
			ProfilePic.sprite =Sprite.Create(result.Texture,new Rect(0,0,128,128),new Vector2());
		}
		else  
		{
			
			
		}
		
	}
	public void FbLabas(){
		
				
			FB.ShareLink
				(new System.Uri("http://dmanager.cherryplay.com.ph/duterteatinto.html"),
				 "Halo-Halo Craze",
				 "Try This Awesome Game",
				 new System.Uri("https://s9.postimg.org/e3ksif0nj/FB_Share_Duterte_Atin_To.png"),
				 callback: ShareCallback
				 );

	}

	private void ShareCallback(IShareResult result)
	{
		if (result.Cancelled || !string.IsNullOrEmpty(result.Error))
		{
			
		}
		else if (!string.IsNullOrEmpty(result.PostId))
		{
			
		}
		else
		{

		}		
	}



	


	public void InviteFriends()
	{
//		FB.AppRequest(
//			message:"This  game is Awesome , join me now",
//			title: "Invite your Friend to Join You"
//		);
		FB.Mobile.AppInvite(
			new System.Uri("https://fb.me/1848781795365574"),
			new System.Uri("https://s9.postimg.org/e3ksif0nj/FB_Share_Duterte_Atin_To.png")

			);
	


	}
	public void LoggedOut()
	{
		FB.LogOut();
		dialogLoggedIn.SetActive(false);
		dialogLoggedOut.SetActive(true);
	}




}
