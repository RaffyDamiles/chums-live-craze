﻿using UnityEngine;
using System.Collections;

public class Notification : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	#if UNITY_ANDROID
	public static void  showNotification( string tittle, string content){
		AndroidJavaClass jClass = new AndroidJavaClass ("arguelles.jonas.unitytesting.MainActivity");
		if (jClass != null) {
			int i = jClass.CallStatic<int>("showNotification",tittle,content);
		}
	}
	#endif

}
