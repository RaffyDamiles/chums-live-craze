﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseBtn : MonoBehaviour {


	public Canvas shopCanvas;
	// Use this for initialization
	void Start () {
		shopCanvas = GameObject.FindWithTag("ShopCanvas").GetComponent<Canvas> ();
		EnableShopCanvas ();
	}


	void EnableShopCanvas()
	{
		if (shopCanvas != null) {

			shopCanvas.enabled = (Time.timeScale == 0);
		}
	}
	// Update is called once per frame
	public 	void TogglePause () {
		if (Time.timeScale != 0) {
		
			Time.timeScale = 0;

		} else {
			Time.timeScale = 1;
		}
		EnableShopCanvas ();
	}
}
